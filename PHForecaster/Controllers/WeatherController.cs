﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PHForecaster.Interfaces;
using PHForecaster.Models;

namespace PHForecaster.Controllers
{
    [Route("api/[controller]")]
    public class WeatherController : Controller
    {
        private readonly IForecasterRepository _repository;

        public WeatherController(IForecasterRepository forecasterRepository)
        {
            _repository = forecasterRepository;
        }
       
        [HttpPost]
        public async Task<ActionResult> GetWeather([FromBody]RequestModel requestModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(requestModel.CityName))
                {
                    var data = await _repository.Get(requestModel.CityName, requestModel.TemperatureScale);
                    var response = new ResponseModel(data, HttpStatusCode.OK);
                    return Ok(response);
                }
                ModelState.AddModelError("CityName", "CityName can't be empty");
                return BadRequest();
            }
            catch(Exception ex)
            {
                return Ok(new ResponseModel(ex.Message, HttpStatusCode.InternalServerError));
            }
        }

        [HttpGet("{cityName}")]
        public async Task<ActionResult> LoadWeather(string cityName)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var data = await _repository.GetFromDb(cityName);
                    return Ok(new ResponseModel(data, HttpStatusCode.OK));
                }
                return BadRequest();
            }
            catch(Exception ex)
            {
                return Ok(new ResponseModel(ex.Message, HttpStatusCode.InternalServerError));
            }
        }

        [HttpPut]
        public async Task<ActionResult> SaveWeather([FromBody]RequestModel requestModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(requestModel.CityName))
                {
                    await _repository.Put(requestModel.CityName, requestModel.TemperatureScale);
                    return Ok(new ResponseModel("Success", HttpStatusCode.OK));
                }
                ModelState.AddModelError("CityName", "CityName can't be empty");
                return BadRequest(ModelState);
            }
            catch(Exception ex)
            {
                return Ok(new ResponseModel(ex.Message, HttpStatusCode.InternalServerError));
            }           
        }
    }
}
