﻿using PHForecaster.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PHForecaster.Interfaces
{
    public interface IForecasterRepository
    {
        Task<List<WeatherUIModel>> Get(string cityName, string temperatureScale);
        Task Put(string cityName, string temperatureScale);
        Task<List<WeatherExtendedUiModel>> GetFromDb(string cityName); 
    }
}
