﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PHForecaster.Constants
{
    public static class TemperatureScales
    {
        public const string Celsius = "metric";
        public const string Kelvin = "";
        public const string Fahrenheits = "imperial";
    }
    public static class OpenWeaterMapConstants
    {
        public const string Key = "openweathermap";
        public const string Url = "https://api.openweathermap.org/data/2.5/";
        public const string ApiKey = "ab612ebf5375d77352246f16e80ad5c7";
    }
    public static class Statuses
    {
        public const string Ok = "OK";
        public const string Fail = "Fail";
    }
}
