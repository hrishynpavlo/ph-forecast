﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using PHForecaster.Constants;
using PHForecaster.Interfaces;
using PHForecaster.Models;
using PHForecaster.Extensions;
using System.Collections.Generic;
using System.Linq;
using PHForecaster.Models.DB;
using Microsoft.EntityFrameworkCore;

namespace PHForecaster.Implementations
{
    public class OpenWeatherMap : IForecasterRepository
    {
        private readonly HttpClient _httpClient;
        private readonly ForecastContext _dbContext;
        public OpenWeatherMap(IHttpClientFactory httpClient, ForecastContext dbContext)
        {
            _httpClient = httpClient.CreateClient(OpenWeaterMapConstants.Key);
            _dbContext = dbContext;
        }
        public async Task<List<WeatherUIModel>> Get(string cityName, string temperatureScale)
        {
            try
            {
                var tscale = GetTemperatureScale(temperatureScale);
                var result = await Task.WhenAll(GetWeather(cityName, tscale), GetForecast(cityName, tscale));
                var forecast = result.SelectMany(s => s).GroupBy(g => g.Day).Select(s => s.FirstOrDefault()).ToList();
                return forecast;
            }
            catch (Exception ex)
            {
                throw new Exception("Error when getting forecast from api", ex);
                //log ex
            }            
        }

        public async Task Put(string cityName, string temperatureScale)
        {
            using(var transaction = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    var tscale = GetTemperatureScale(temperatureScale);
                    var response = await Get(cityName, tscale);
                    var addModel = response.Select(s => s.ConvertForecastUiModelToDbModel(cityName, tscale));
                    await _dbContext.AddRangeAsync(addModel);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    //log ex
                    throw new Exception("Error when putting weather to db", ex);
                }
            }
        }

        public async Task<List<WeatherExtendedUiModel>> GetFromDb(string cityName)
        {
            try
            {
                return await _dbContext.Forecasts.Include(i => i.Clouds).Include(i => i.Wind).AsNoTracking().Where(x => x.City == cityName).Select(s => s.ConvertForecastDbModelToUiModel()).ToListAsync();
            }
            catch(Exception ex)
            {
                //log ex
                throw new Exception("Error when getting data from db", ex);
            }
        }

        private async Task<List<WeatherUIModel>> GetWeather(string cityName, string temperatureScale)
        {
            try
            {
                var weather = new WeatherUIModel();
                var request = new HttpRequestMessage(HttpMethod.Get, $"weather?q={cityName}&appid={OpenWeaterMapConstants.ApiKey}&units={temperatureScale}");
                var response = await _httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsAsync<WeatherResponseModel>();
                    weather = result.ConvertToUIModel();
                }
                return new List<WeatherUIModel>() { weather };
            }
            catch (Exception ex)
            {
                //log ex   
                throw new Exception("Error when getting weather from api", ex);
            }
        }

        private async Task<List<WeatherUIModel>> GetForecast(string cityName, string temperatureScale)
        {
            try
            {
                var forecast = new List<WeatherUIModel>();
                var request = new HttpRequestMessage(HttpMethod.Get, $"forecast?q={cityName}&appid={OpenWeaterMapConstants.ApiKey}&units={temperatureScale}");
                var response = await _httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsAsync<ForecastReponseModel>();
                    forecast = result.list.Select(s => s.ConvertToUIModel()).ToList();
                }
                return forecast;
            }
            catch(Exception ex)
            {
                throw new Exception("Error when getting forecast from api", ex);
                //log ex   
            }
        }

        private string GetTemperatureScale(string uiScale)
        {
            switch (uiScale)
            {
                case "Kelvin":
                    return TemperatureScales.Kelvin;
                case "Celsius":
                    return TemperatureScales.Celsius;
                case "Fahrenheits":
                    return TemperatureScales.Fahrenheits;
                default:
                    throw new Exception("Current temparature scale doen't exist.");
            }
        }
    }
}
