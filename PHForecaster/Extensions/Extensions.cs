﻿using System;
using PHForecaster.Models;

namespace PHForecaster.Extensions
{
    public static class Extensions
    {
        public static WeatherUIModel ConvertToUIModel(this WeatherResponseModel responseModel)
        {
            return new WeatherUIModel
            {
                Day = responseModel.DtTxt.HasValue ? responseModel.DtTxt.Value.ToString("dd-MMM-yyyy") : DateTime.UtcNow.ToString("dd-MMM-yyyy"),
                Temp = responseModel.Main.temp,
                TempMin = responseModel.Main.temp_min,
                TempMax = responseModel.Main.temp_max,
                Pressure = responseModel.Main.preassure,
                Humidity = responseModel.Main.humidity,
                Wind = responseModel.Wind,
                Clouds = responseModel.Clouds
            };
        }

        public static Models.DB.Forecast ConvertForecastUiModelToDbModel(this WeatherUIModel uiModel, string cityName, string temperatureScale)
        {
            return new Models.DB.Forecast
            {
                City = cityName,
                TemperatureScale = temperatureScale,
                Day = uiModel.Day,
                Clouds = new Models.DB.Clouds { All = uiModel.Clouds.all },
                Humidity = uiModel.Humidity,
                Pressure = uiModel.Pressure,
                Temp = uiModel.Temp,
                TempMax = uiModel.TempMax,
                TempMin = uiModel.TempMin,
                Wind = new Models.DB.Wind { Deg = uiModel.Wind.deg, Speed = uiModel.Wind.speed }
            };
        }

        public static WeatherExtendedUiModel ConvertForecastDbModelToUiModel(this Models.DB.Forecast dbForecast)
        {
            return new WeatherExtendedUiModel
            {
                Day = dbForecast.Day,
                Humidity = dbForecast.Humidity,
                Clouds = new Clouds { all = dbForecast.Clouds.All },
                Pressure = dbForecast.Pressure,
                Temp = dbForecast.Temp,
                TempMax = dbForecast.TempMax,
                TempMin = dbForecast.TempMin,
                Wind = new Wind { deg = dbForecast.Wind.Deg, speed = dbForecast.Wind.Speed },
                CityName = dbForecast.City,
                TemperatureScale = dbForecast.TemperatureScale
            };
        }
    }
}