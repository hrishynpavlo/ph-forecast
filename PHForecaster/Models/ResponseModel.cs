﻿using System.Net;

namespace PHForecaster.Models
{
    public class ResponseModel
    {
        public object Data { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public ResponseModel(string message, HttpStatusCode statusCode) {
            Message = message;
            StatusCode = (int)statusCode;
        }
        public ResponseModel(object data, HttpStatusCode statusCode)
        {
            Data = data;
            StatusCode = (int)statusCode;
        }
    }
}
