﻿using Microsoft.EntityFrameworkCore;
using System;

namespace PHForecaster.Models.DB
{
    public class ForecastContext : DbContext
    {
        private string _connectionString;
        public DbSet<Forecast> Forecasts { get; set; }

        public ForecastContext()
        {
            Database.EnsureCreated();
        }

        public ForecastContext(DbContextOptions<ForecastContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Environment.GetEnvironmentVariable("DB_CONNECTION_STRING"));

            //for migrations
            //optionsBuilder.UseSqlite("Data Source=WeatherDB.db;");

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Forecast>().HasKey(k => k.Id);
            modelBuilder.Entity<Forecast>().HasIndex(i => i.City);
            modelBuilder.Entity<Forecast>().HasOne(o => o.Wind).WithOne(o => o.Forecast).HasForeignKey<Forecast>(f => f.WindId);
            modelBuilder.Entity<Forecast>().HasOne(o => o.Clouds).WithOne(o => o.Forecast).HasForeignKey<Forecast>(f => f.CloudsId);
        }
    }

    public class Forecast
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string TemperatureScale { get; set; }
        public string Day { get; set; }
        public double Temp { get; set; }
        public double TempMin { get; set; }
        public double TempMax { get; set; }
        public int Pressure { get; set; }
        public int Humidity { get; set; }
        public int WindId { get; set; }
        public DB.Wind Wind { get; set; }
        public int CloudsId { get; set; }
        public DB.Clouds Clouds { get; set; }
    }
    public class Wind
    {
        public int Id { get; set; }
        public double Speed { get; set; }
        public double Deg { get; set; }
        public Forecast Forecast { get; set; }
    }
    public class Clouds
    {
        public int Id { get; set; }
        public int All { get; set; }
        public Forecast Forecast { get; set; }
    }
}
