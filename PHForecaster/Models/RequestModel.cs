﻿using PHForecaster.Constants;

namespace PHForecaster.Models
{
    public class RequestModel
    {        
        public string CityName { get; set; }       
        public string TemperatureScale { get; set; }
    }
}
