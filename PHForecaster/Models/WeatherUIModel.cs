﻿using Newtonsoft.Json;

namespace PHForecaster.Models
{
    public class WeatherUIModel
    {
        [JsonProperty("day")]
        public string Day { get; set; }
        [JsonProperty("temp")]
        public double Temp { get; set; }
        [JsonProperty("temp_min")]
        public double TempMin { get; set; }
        [JsonProperty("temp_max")]
        public double TempMax { get; set; }
        [JsonProperty("pressure")]
        public int Pressure { get; set; }
        [JsonProperty("humidity")]
        public int Humidity { get; set; }
        [JsonProperty("wind")]
        public Wind Wind { get; set; }
        [JsonProperty("clouds")]
        public Clouds Clouds { get; set; }
    }
    public class WeatherExtendedUiModel : WeatherUIModel
    {
        [JsonProperty("city_name")]
        public string CityName { get; set; }
        [JsonProperty("temperature_scale")]
        public string TemperatureScale { get; set; }
    }
}
