﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace PHForecaster.Models
{
    public class ForecastReponseModel
    {
        public List<WeatherResponseModel> list { get; set; }
    }
    public class WeatherResponseModel
    {
        public Coord Coord { get; set; }
        public List<Weather> Weather { get; set; }
        public string Base { get; set; }
        public Main Main { get; set; }
        public int Vsisibility { get; set; }
        public Wind Wind { get; set; }
        public Clouds Clouds { get; set; }
        public string Dt { get; set; }
        public Sys Sys { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public int Cod { get; set; }
        [JsonProperty("dt_txt")]
        public DateTime? DtTxt { get; set; }
    }
    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }
    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }
    public class Main
    {
        public double temp { get; set; }
        public int preassure { get; set; }
        public int humidity { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
    }
    public class Wind
    {
        public double speed { get; set; }
        public double deg { get; set; }
    }
    public class Clouds
    {
        public int all { get; set; }
    }
    public class Sys
    {
        public int type { get; set; }
        public int id { get; set; }
        public double message { get; set; }
        public string country { get; set; }
        public long sunrise { get; set; }
        public long sunset { get; set; }
    }
}
