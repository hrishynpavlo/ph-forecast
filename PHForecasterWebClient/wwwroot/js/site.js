﻿$(document).ready(function () {

    var city;
    var scale;
    var forecasts = [];

    var table = $("#forecast").DataTable({
        searching: false,
        paging: false,
        data: forecasts,
        columns: [
            { data: 'city_name' },
            { data: 'temperature_scale' },
            { data: 'day', "width": "10%" },
            { data: 'temp' },
            { data: 'temp_min' },
            { data: 'temp_max' },
            { data: 'pressure' },
            { data: 'humidity' },
            { data: 'wind.speed' },
            { data: 'wind.deg' },
            { data: 'clouds.all' }
        ]
    });

    $(".searchButton").click(function () {
        RunSpinner();
        city = $("#cityName").val();
        scale = $("#temperatureScale").val()
        var reqData = {
            CityName: city,
            TemperatureScale: scale
        };
        var data = JSON.stringify(reqData);
        $.ajax({
            type: 'POST',
            url: 'http://localhost:60028/api/Weather',
            dataType: 'json',
            contentType: 'application/json',
            data: data,
            success: function (response) {

                forecasts = response.data.map(e => {
                    return Object.assign(e, { city_name: city })
                });

                forecasts = response.data.map(e => {
                    return Object.assign(e, { temperature_scale: scale })
                });

                table.clear();
                table.rows.add(forecasts);
                table.draw();
                StopSpinner();
            },
            error: function () {
                console.log("Device control failed");
                StopSpinner();
            },
        });
    });

    function RunSpinner() {
        var top = $("#searchForm").offset().top;
        var left = $("#searchForm").offset().left;
        var width = $("#searchForm").width();
        var height = $("#searchForm").height();
        $("#spinner").offset({ top: top + height / 2, left: left + width / 2 });
        $("#spinner").css('visibility', 'visible');
        $("#searchForm").css("pointer-events", "none");
    }

    function StopSpinner() {
        $("#spinner").css('visibility', 'hidden');
        $("#searchForm").css("pointer-events", "auto");
    }
});